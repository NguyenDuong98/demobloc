// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'work.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WorkModel _$WorkModelFromJson(Map<String, dynamic> json) {
  return WorkModel(
      id: json['id'] as int,
      date: json['date'] as String,
      time: json['time'] as String,
      content: json['content'] as String,
      color: json['color'] as String);
}

Map<String, dynamic> _$WorkModelToJson(WorkModel instance) => <String, dynamic>{
      'id': instance.id,
      'date': instance.date,
      'time': instance.time,
      'content': instance.content,
      'color': instance.color
    };
