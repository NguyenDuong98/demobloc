import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
part 'work.model.g.dart';

@JsonSerializable(includeIfNull: true)
class WorkModel {
  int id;
  String date;
  String time;
  String content;
  String color;

  WorkModel({this.id, this.date, this.time, this.content, this.color});

  factory WorkModel.fromJson(Map<String, dynamic> json) =>
      _$WorkModelFromJson(json);
  Map<String, dynamic> toJson() => _$WorkModelToJson(this);
}
