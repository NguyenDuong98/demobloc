part of 'login.bloc.dart';

class LoginFormState extends Equatable {
  const LoginFormState({
    this.email = const Email.pure(),
    this.errorEmail = '',
    this.password = const Password.pure(),
    this.errorPassword = '',
    this.status = FormzStatus.pure,
  });

  final Email email;
  final String errorEmail;
  final Password password;
  final String errorPassword;
  final FormzStatus status;

  LoginFormState copyWith({
    Email email,
    String errorEmail,
    Password password,
    String errorPassword,
    FormzStatus status,
  }) {
    return LoginFormState(
      email: email ?? this.email,
      errorEmail: errorEmail ?? this.errorEmail,
      password: password ?? this.password,
      errorPassword: errorPassword ?? this.errorPassword,
      status: status ?? this.status,
    );
  }

  @override
  List<Object> get props =>
      [email, errorEmail, password, status, errorPassword];
}
