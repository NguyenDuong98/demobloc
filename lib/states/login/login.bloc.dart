import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:formz/formz.dart';
import 'package:equatable/equatable.dart';

import 'package:todoBloc/controllers/authen.controller.dart';
import 'package:todoBloc/validate/login.validate.dart';

part 'login.event.dart';
part 'login.state.dart';

class LoginFormBloc extends Bloc<LoginFormEvent, LoginFormState> {
  LoginFormBloc() : super(const LoginFormState());
  AuthenController authenController = AuthenController();

  @override
  void onTransition(Transition<LoginFormEvent, LoginFormState> transition) {
    // print(transition);
    super.onTransition(transition);
  }

  @override
  Stream<LoginFormState> mapEventToState(LoginFormEvent event) async* {
    if (event is EmailChanged) {
      final email = Email.dirty(event.email);
      yield state.copyWith(
        email: email.valid ? email : Email.pure(event.email),
        errorEmail: email.valid ? '' : 'Email không hợp lệ',
        status: Formz.validate([email, state.password]),
      );
    } else if (event is PasswordChanged) {
      final password = Password.dirty(event.password);
      yield state.copyWith(
        password: password.valid ? password : Password.pure(event.password),
        errorPassword: password.valid ? '' : 'Pasword không hợp lệ',
        status: Formz.validate([state.email, password]),
      );
    } else if (event is EmailUnfocused) {
      final email = Email.dirty(state.email.value);
      yield state.copyWith(
        email: email,
        status: Formz.validate([email, state.password]),
      );
    } else if (event is PasswordUnfocused) {
      final password = Password.dirty(state.password.value);
      yield state.copyWith(
        password: password,
        status: Formz.validate([state.email, password]),
      );
    } else if (event is LoginSubmitted) {
      final email = Email.dirty(state.email.value);
      final password = Password.dirty(state.password.value);
      yield state.copyWith(
        email: email,
        password: password,
        status: Formz.validate([email, password]),
      );
      if (state.status.isValidated) {
        final response = await authenController.login(
            state.email.value, state.password.value);
        if (!response) {
          yield state.copyWith(
              errorEmail: 'Vui lòng kiểm tra lại email',
              errorPassword: 'Vui lòng kiểm tra lại password');
        }
      }
    } else if (event is CreateAccountSubmitted) {
      final email = Email.dirty(state.email.value);
      final password = Password.dirty(state.password.value);
      yield state.copyWith(
        email: email,
        password: password,
        status: Formz.validate([email, password]),
      );
      if (state.status.isValidated) {
        Map data = {
          'email': state.email.value,
          'password': state.password.value
        };
        authenController.createUser(data);
      }
    }
  }
}
