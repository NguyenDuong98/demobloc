part of 'login.bloc.dart';

abstract class LoginFormEvent extends Equatable {
  const LoginFormEvent();
  List<Object> get props => [];
}

// ------------------- Email State ------------------- //
class EmailChanged extends LoginFormEvent {
  final String email;
  const EmailChanged({this.email});

  @override
  List<Object> get props => [email];
}

class EmailUnfocused extends LoginFormEvent {}  

// ------------------- Password State ------------------- //
class PasswordChanged extends LoginFormEvent {
  final String password;
  const PasswordChanged({this.password});

  @override
  List<Object> get props => [password];
}

class PasswordUnfocused extends LoginFormEvent {}

// ------------------- Form Event ------------------- //
class LoginSubmitted extends LoginFormEvent {}

class CreateAccountSubmitted extends LoginFormEvent {}
