part of 'home.bloc.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();
  List<Object> get props => [];
}

class GetWorks extends HomeEvent {
  final List<WorkModel> works;
  final String test;
  const GetWorks({this.works, this.test});

  @override
  List<Object> get props => [works];
}

class AddWork extends HomeEvent {
  final WorkModel work;

  const AddWork({this.work});

  @override
  List<Object> get props => [work];
}
