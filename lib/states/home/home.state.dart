part of 'home.bloc.dart';

class HomeState extends Equatable {
  const HomeState({
    this.works = const [],
    this.test = '',
  });

  final List<WorkModel> works;
  final String test;

  HomeState copyWith({List<WorkModel> works, String test}) {
    return HomeState(works: works ?? this.works, test: test ?? this.test);
  }

  @override
  List<Object> get props => [works, test];
}
