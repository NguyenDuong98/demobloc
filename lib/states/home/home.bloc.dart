import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:formz/formz.dart';
import 'package:equatable/equatable.dart';

import 'package:todoBloc/controllers/authen.controller.dart';
import 'package:todoBloc/models/work.model.dart';
import 'package:todoBloc/validate/login.validate.dart';

part 'home.event.dart';
part 'home.state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(const HomeState());
  AuthenController authenController = AuthenController();

  @override
  void onTransition(Transition<HomeEvent, HomeState> transition) {
    super.onTransition(transition);
  }

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is GetWorks) {
      yield state.copyWith(works: event.works, test: event.test);
    } else if (event is AddWork) {
      List<WorkModel> works = [...state.works, event.work];
      print(works);
      yield state.copyWith(
        works: works,
      );
      print('Nhi');
      print(state.works);
    }
  }
}
