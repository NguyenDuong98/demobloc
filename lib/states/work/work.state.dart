part of 'work.bloc.dart';

class WorkFormState extends Equatable {
  WorkFormState({
    DateTime date,
    this.time: const TimeOfDay(hour: 12, minute: 00),
    this.content = const Content.pure(),
    this.color = const Color(0xFFE53935),
    this.errorContent = '',
    this.status = FormzStatus.pure,
  }) : this.date = date ?? DateTime.now();

  final DateTime date;
  final TimeOfDay time;
  final Content content;
  final Color color;
  final String errorContent;
  final FormzStatus status;

  WorkFormState copyWith({
    DateTime date,
    TimeOfDay time,
    Content content,
    Color color,
    String errorContent,
    FormzStatus status,
  }) {
    return WorkFormState(
      date: date ?? this.date,
      time: time ?? this.time,
      content: content ?? this.content,
      color: color ?? this.color,
      errorContent: errorContent ?? this.errorContent,
      status: status ?? this.status,
    );
  }

  @override
  List<Object> get props => [date, time, content, color, errorContent, status];
}
