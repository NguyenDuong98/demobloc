part of 'work.bloc.dart';

abstract class WorkFormEvent extends Equatable {
  const WorkFormEvent();
  List<Object> get props => [];
}

class DateChanged extends WorkFormEvent {
  final DateTime date;
  const DateChanged({this.date});

  @override
  List<Object> get props => [date];
}

class TimeChanged extends WorkFormEvent {
  final TimeOfDay time;
  const TimeChanged({this.time});

  @override
  List<Object> get props => [time];
}

class ContentChanged extends WorkFormEvent {
  final String content;
  const ContentChanged({this.content});

  @override
  List<Object> get props => [content];
}

class ColorChanged extends WorkFormEvent {
  final Color color;
  const ColorChanged({this.color});

  @override
  List<Object> get props => [color];
}

class CreateWorkEvent extends WorkFormEvent {}
