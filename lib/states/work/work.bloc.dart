import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:equatable/equatable.dart';

import 'package:todoBloc/controllers/work.controller.dart';
import 'package:todoBloc/validate/work.validate.dart';

part 'work.event.dart';
part 'work.state.dart';

class WorkFormBloc extends Bloc<WorkFormEvent, WorkFormState> {
  WorkFormBloc() : super(WorkFormState());
  WorkController workController = WorkController();

  @override
  void onTransition(Transition<WorkFormEvent, WorkFormState> transition) {
    super.onTransition(transition);
  }

  @override
  Stream<WorkFormState> mapEventToState(WorkFormEvent event) async* {
    if (event is DateChanged) {
      yield state.copyWith(date: event.date);
    } else if (event is TimeChanged) {
      yield state.copyWith(time: event.time);
    } else if (event is ContentChanged) {
      final content = Content.dirty(event.content);
      yield state.copyWith(
        content: content.valid ? content : Content.pure(event.content),
        errorContent: content.invalid ? '' : 'Nội dung là bắt buộc',
        status: Formz.validate([state.content]),
      );
    } else if (event is ColorChanged) {
      yield state.copyWith(color: event.color);
    } else if (event is CreateWorkEvent) {
      if (state.errorContent == '') {
        Map data = {
          'date': state.date.toString(),
          'time': '${state.time.hour}:${state.time.minute}',
          'content': state.content.value,
          'color': state.color.toString()
        };
        workController.createWork(data);
      }
    }
  }
}
