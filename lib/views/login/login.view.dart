import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todoBloc/databases/user.database.dart';

import 'package:todoBloc/states/login/login.bloc.dart';
import 'package:todoBloc/views/login/animation.view.dart';

class Login extends StatefulWidget {
  Login({Key key});

  @override
  State<StatefulWidget> createState() => new _LoginState();
}

class _LoginState extends State<Login> {
  bool existAccount = false;
  UserDatabase userDatabase = UserDatabase();
  final _emailFocusNode = FocusNode();
  final _passwordFocusNode = FocusNode();

  _getAccount() {
    userDatabase.getUser().then((value) => {
          if (value.isNotEmpty)
            {
              setState(() {
                existAccount = true;
              })
            }
        });
  }

  @override
  void initState() {
    super.initState();
    _getAccount();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: BlocProvider(
            create: (_) => LoginFormBloc(),
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 400,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/images/background.png'),
                              fit: BoxFit.fill)),
                      child: Stack(
                        children: <Widget>[
                          Positioned(
                            left: 30,
                            width: 80,
                            height: 200,
                            child: FadeAnimation(
                                1,
                                Container(
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/light-1.png'))),
                                )),
                          ),
                          Positioned(
                            left: 140,
                            width: 80,
                            height: 150,
                            child: FadeAnimation(
                                1.3,
                                Container(
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/light-2.png'))),
                                )),
                          ),
                          Positioned(
                            right: 40,
                            top: 40,
                            width: 80,
                            height: 150,
                            child: FadeAnimation(
                                1.5,
                                Container(
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/clock.png'))),
                                )),
                          ),
                          Positioned(
                            child: FadeAnimation(
                                1.6,
                                Container(
                                  margin: EdgeInsets.only(top: 50),
                                  child: Center(
                                    child: Text(
                                      "Login",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 40,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                )),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(30.0),
                      child: Column(
                        children: <Widget>[
                          FadeAnimation(
                              1.8,
                              Container(
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                          color:
                                              Color.fromRGBO(143, 148, 251, .2),
                                          blurRadius: 20.0,
                                          offset: Offset(0, 10))
                                    ]),
                                child:
                                    BlocBuilder<LoginFormBloc, LoginFormState>(
                                        builder: (context, state) {
                                  return Column(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.all(8.0),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            border: Border(
                                                bottom: BorderSide(
                                                    color: Colors.grey[100]))),
                                        child: TextField(
                                          focusNode: _emailFocusNode,
                                          decoration: InputDecoration(
                                              border: InputBorder.none,
                                              errorText: state.errorEmail == ''
                                                  ? null
                                                  : state.errorEmail,
                                              hintText: "Email or Phone number",
                                              hintStyle: TextStyle(
                                                  color: Colors.grey[400])),
                                          onChanged: (value) {
                                            context.read<LoginFormBloc>().add(
                                                EmailChanged(email: value));
                                          },
                                        ),
                                      ),
                                      Container(
                                        color: Colors.white,
                                        padding: EdgeInsets.all(8.0),
                                        margin: EdgeInsets.only(top: 10),
                                        child: TextField(
                                            focusNode: _passwordFocusNode,
                                            decoration: InputDecoration(
                                                border: InputBorder.none,
                                                hintText: "Password",
                                                errorText:
                                                    state.errorPassword == ''
                                                        ? null
                                                        : state.errorPassword,
                                                hintStyle: TextStyle(
                                                    color: Colors.grey[400])),
                                            onChanged: (value) {
                                              context.read<LoginFormBloc>().add(
                                                  PasswordChanged(
                                                      password: value));
                                            }),
                                      ),
                                      SizedBox(
                                        height: 30,
                                      ),
                                      FadeAnimation(
                                          2,
                                          InkWell(
                                              onTap: () {
                                                existAccount
                                                    ? context
                                                        .read<LoginFormBloc>()
                                                        .add(LoginSubmitted())
                                                    : context
                                                        .read<LoginFormBloc>()
                                                        .add(
                                                            CreateAccountSubmitted());
                                              },
                                              child: Container(
                                                height: 50,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    gradient:
                                                        LinearGradient(colors: [
                                                      Color.fromRGBO(
                                                          143, 148, 251, 1),
                                                      Color.fromRGBO(
                                                          143, 148, 251, .6),
                                                    ])),
                                                child: Center(
                                                  child: Text(
                                                    existAccount
                                                        ? "Login"
                                                        : "Create Account",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                              ))),
                                      SizedBox(
                                        height: 70,
                                      ),
                                    ],
                                  );
                                }),
                              )),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )));
  }
}
