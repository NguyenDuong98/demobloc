import 'package:flutter/material.dart';
import 'package:day_night_time_picker/day_night_time_picker.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todoBloc/models/work.model.dart';
import 'package:todoBloc/states/home/home.bloc.dart';
import 'package:todoBloc/states/work/work.bloc.dart';

class CreateWork extends StatefulWidget {
  CreateWork({Key key}) : super(key: key);

  @override
  _CreateWorkState createState() => _CreateWorkState();
}

class _CreateWorkState extends State<CreateWork> {
  DateTime nowDate = DateTime.now();

  List<Color> tags = [
    Color(0xFFE53935),
    Color(0xFF43A047),
    Color(0xFFFB8C00),
    Color(0xFF8E24AA)
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(builder: (context, setState) {
      return AlertDialog(
        title: Text('Thêm Công Việc'),
        insetPadding: EdgeInsets.only(left: 10, right: 10),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        content: Builder(
          builder: (context) {
            double width = MediaQuery.of(context).size.width;

            return Container(
                width: width,
                child: SingleChildScrollView(
                    child: BlocProvider(
                        create: (_) => WorkFormBloc(),
                        child: BlocBuilder<WorkFormBloc, WorkFormState>(
                            builder: (context, state) {
                          return Column(
                            children: [
                              InkWell(
                                  onTap: () async {
                                    final DateTime picked =
                                        await showDatePicker(
                                            context: context,
                                            initialDate: nowDate,
                                            firstDate:
                                                DateTime(nowDate.year - 1),
                                            lastDate: DateTime(2101));
                                    if (picked != null && picked != nowDate) {
                                      context
                                          .read<WorkFormBloc>()
                                          .add(DateChanged(date: picked));
                                    }
                                  },
                                  child: Container(
                                    height: 50,
                                    width: width,
                                    alignment: Alignment.centerLeft,
                                    padding: EdgeInsets.only(left: 5),
                                    margin: EdgeInsets.only(bottom: 10),
                                    decoration: BoxDecoration(
                                        border: Border.all(color: Colors.grey),
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Row(children: [
                                      Expanded(
                                          child: Text(
                                              '${state.date.day}/${state.date.month}/${state.date.year}',
                                              style: TextStyle(fontSize: 20))),
                                      Container(
                                          child: Icon(Icons.arrow_drop_down))
                                    ]),
                                  )),
                              InkWell(
                                  onTap: () {
                                    Navigator.of(context).push(
                                      showPicker(
                                        context: context,
                                        value: TimeOfDay(
                                            hour: DateTime.now().hour,
                                            minute: DateTime.now().minute),
                                        is24HrFormat: true,
                                        disableHour: false,
                                        disableMinute: false,
                                        onChange: (time) {
                                          context
                                              .read<WorkFormBloc>()
                                              .add(TimeChanged(time: time));
                                        },
                                      ),
                                    );
                                  },
                                  child: Container(
                                      height: 50,
                                      width: double.infinity,
                                      padding: EdgeInsets.only(left: 5),
                                      alignment: Alignment.centerLeft,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          border:
                                              Border.all(color: Colors.grey)),
                                      child: Text(
                                          '${state.time.hour} : ${state.time.minute}',
                                          style: TextStyle(fontSize: 20)))),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: TextField(
                                  maxLines: 3,
                                  // controller: txtContent,
                                  onChanged: (newValue) {
                                    context
                                        .read<WorkFormBloc>()
                                        .add(ContentChanged(content: newValue));
                                  },
                                  decoration: new InputDecoration(
                                      errorText: state.errorContent == ''
                                          ? null
                                          : state.errorContent,
                                      border: new OutlineInputBorder(
                                          borderSide: new BorderSide(
                                              color: Colors.teal)),
                                      labelText: 'Nội Dung',
                                      prefixText: ' ',
                                      suffixStyle:
                                          const TextStyle(color: Colors.green)),
                                ),
                              ),
                              Container(
                                  alignment: Alignment.topLeft,
                                  margin: EdgeInsets.only(top: 10),
                                  child: Text('Chọn Tag')),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Row(
                                  children: []..addAll(buildListTag()),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 20),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Container(
                                        height: 40,
                                        padding: EdgeInsets.only(
                                            top: 5,
                                            bottom: 5,
                                            left: 10,
                                            right: 10),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                            color: Colors.grey[400],
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: Text('Hủy',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w600)),
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        context
                                            .read<WorkFormBloc>()
                                            .add(CreateWorkEvent());
                                        context.read<HomeBloc>().add(AddWork(
                                            work: WorkModel(
                                                color: Colors.red.toString(),
                                                date: DateTime.now().toString(),
                                                time:
                                                    TimeOfDay.now().toString(),
                                                content: 'duong')));
                                      },
                                      child: Container(
                                        height: 40,
                                        margin: EdgeInsets.only(left: 10),
                                        padding: EdgeInsets.only(
                                            top: 5,
                                            bottom: 5,
                                            left: 10,
                                            right: 10),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                            color: Colors.blue[300],
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: Text('Thêm Công Việc',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w600)),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          );
                        }))));
          },
        ),
      );
    });
  }

  List<Widget> buildListTag() {
    return tags.map((tag) => buildTag(tag)).toList();
  }

  Widget buildTag(Color tag) {
    return BlocBuilder<WorkFormBloc, WorkFormState>(builder: (context, state) {
      return Container(
          width: 50,
          height: 50,
          margin: EdgeInsets.only(right: 10),
          decoration: BoxDecoration(
              color: tag,
              borderRadius: BorderRadius.circular(100),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0, 0),
                  blurRadius: 6,
                ),
              ]),
          child: InkWell(
            onTap: () {
              context.read<WorkFormBloc>().add(ColorChanged(color: tag));
            },
            child: state.color == tag
                ? Icon(
                    Icons.check,
                    size: 40,
                    color: Colors.white,
                  )
                : Container(),
          ));
    });
  }
}
