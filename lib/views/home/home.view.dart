import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

import 'package:todoBloc/controllers/work.controller.dart';
import 'package:todoBloc/databases/work.database.dart';
import 'package:todoBloc/models/work.model.dart';
import 'package:todoBloc/utilitys/general.utility.dart';
import 'package:todoBloc/views/home/work.view.dart';
import 'package:todoBloc/states/home/home.bloc.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<WorkModel> works = [];
  HomeBloc homeBloc;
  WorkDatabase workDatabase = WorkDatabase();
  GeneralUtility generalUtility = GeneralUtility();
  WorkController workController = WorkController();

  _getWorks() async {
    await workController.getWorks().then((value) => {
          setState(() => works = value),
          // homeBloc = HomeBloc(),
          // homeBloc.add(GetWorks(works: works, test: 'Dương')),
        });
  }

  List<Appointment> _getCalendarDataSource(List<WorkModel> listWork) {
    List<Appointment> appointments = <Appointment>[];
    final now = new DateTime.now();
    listWork.forEach((element) {
      TimeOfDay time = TimeOfDay(
          hour: int.parse(element.time.split(":")[0]),
          minute: int.parse(element.time.split(":")[1]));
      final startTime =
          DateTime(now.year, now.month, now.day, time.hour, time.minute);
      final endTime =
          DateTime(now.year, now.month, now.day, time.hour + 1, time.minute);
      appointments.add(Appointment(
          startTime: startTime,
          endTime: endTime,
          isAllDay: false,
          subject: '${element.content}',
          color: generalUtility.getColor(element.color),
          notes: '${element.content}'));
    });

    return appointments;
  }

  @override
  void initState() {
    super.initState();
    _getWorks();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;

    return SafeArea(
        child: BlocProvider(
            create: (_) => HomeBloc(),
            child: BlocBuilder<HomeBloc, HomeState>(
              builder: (context, state) {
                context.read<HomeBloc>().add(GetWorks(works: works));
                return Scaffold(
                    body: Container(
                        color: Colors.grey[400].withOpacity(0.5),
                        padding: EdgeInsets.only(
                            left: 10, right: 10, top: 20, bottom: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                                child: Container(
                                    height: height * 0.3,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.grey,
                                              blurRadius: 20.0,
                                              offset: Offset(0, 10))
                                        ]),
                                    child: SfCalendar(
                                        headerStyle: CalendarHeaderStyle(
                                            textStyle: TextStyle(
                                                fontSize: 20,
                                                color: Colors.white),
                                            backgroundColor: Colors.blue[500]),
                                        viewHeaderStyle: ViewHeaderStyle(
                                            backgroundColor: Colors.blue[500],
                                            dayTextStyle: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold)),
                                        backgroundColor:
                                            Colors.white.withOpacity(0.8),
                                        view: CalendarView.month,
                                        dataSource: DataSource(
                                            _getCalendarDataSource(
                                                state.works)),
                                        monthViewSettings: MonthViewSettings(
                                          showTrailingAndLeadingDates: false,
                                          appointmentDisplayMode:
                                              MonthAppointmentDisplayMode
                                                  .indicator,
                                          showAgenda: true,
                                          agendaViewHeight: height * 0.4,
                                        ))))
                          ],
                        )),
                    floatingActionButton: FloatingActionButton(
                      onPressed: () {
                        showDialog(
                            context: context, builder: (_) => CreateWork());
                      },
                      child: const Icon(Icons.calendar_today),
                      backgroundColor: Colors.green,
                    ));
              },
            )));
  }
}

class DataSource extends CalendarDataSource {
  DataSource(List<Appointment> source) {
    appointments = source;
  }
}
