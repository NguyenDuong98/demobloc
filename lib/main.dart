import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todoBloc/views/home/home.view.dart';
import 'package:todoBloc/views/login/login.view.dart';

void main() {
  runApp(GetMaterialApp(home: Home()));
}
