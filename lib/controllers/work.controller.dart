import 'package:todoBloc/models/work.model.dart';
import 'package:todoBloc/services/work.service.dart';

class WorkController {
  WorkService workService = WorkService();

  Future<void> createWork(Map data) async {
    return await workService.createWork(data);
  }

  Future<List<WorkModel>> getWorks() async {
    return await workService.getWorks();
  }
}
