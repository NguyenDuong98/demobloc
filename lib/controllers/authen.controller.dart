import 'package:todoBloc/services/authen.service.dart';

class AuthenController {
  AuthenService authenService = AuthenService();

  Future<void> createUser(Map data) async {
    return await authenService.createUser(data);
  }

  Future<bool> login(String email, String password) async {
    return await authenService.login(email, password);
  }
}
