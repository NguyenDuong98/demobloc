import 'dart:convert';
import 'package:get/get.dart';

import 'package:todoBloc/databases/user.database.dart';
import 'package:todoBloc/views/home/home.view.dart';

class AuthenService {
  UserDatabase userDatabase = UserDatabase();

  Future<void> createUser(Map data) async {
    userDatabase.insertUser(jsonDecode(jsonEncode(data)));
    Get.to(Home());
    return true;
  }

  Future<bool> login(String email, String password) async {
    final response = await userDatabase.login(email, password);
    if (response.isNotEmpty) {
      Get.to(Home());
      return true;
    }

    return false;
  }
}
