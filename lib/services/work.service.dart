import 'dart:convert';
import 'package:get/get.dart';

import 'package:todoBloc/databases/work.database.dart';
import 'package:todoBloc/models/work.model.dart';
import 'package:todoBloc/states/home/home.bloc.dart';
import 'package:todoBloc/utilitys/general.utility.dart';

class WorkService {
  HomeBloc homeBloc;
  WorkDatabase workDatabase = WorkDatabase();
  GeneralUtility generalUtility = GeneralUtility();

  Future<void> createWork(Map data) async {
    WorkModel newWork = WorkModel.fromJson(jsonDecode(jsonEncode(data)));
    // workDatabase.insertWork(jsonDecode(jsonEncode(data)));
    homeBloc = HomeBloc();
    homeBloc.add(AddWork(work: newWork));
    Get.back();
    generalUtility.showNotification('Thêm Công Việc Thành Công');
    return true;
  }

  Future<List<WorkModel>> getWorks() async {
    final response = await workDatabase.getWorks();
    List<WorkModel> works = [];
    response.forEach((work) {
      works.add(WorkModel.fromJson(jsonDecode(jsonEncode(work))));
    });
    return works;
  }
}
