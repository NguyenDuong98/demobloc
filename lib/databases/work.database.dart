import 'package:sqflite/sqflite.dart';

class WorkDatabase {
  static Database _database;
  static String table = 'Work';
  static String id = 'id';
  static String date = 'date';
  static String time = 'time';
  static String content = 'content';
  static String color = 'color';
  static WorkDatabase _workDatabase;

  WorkDatabase._createInstance();

  factory WorkDatabase() {
    if (_workDatabase == null) {
      _workDatabase = WorkDatabase._createInstance();
    }
    return _workDatabase;
  }

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await initDB();
    return _database;
  }

  Future<Database> initDB() async {
    var dir = await getDatabasesPath();
    var path = dir + 'work.db';

    var database = await openDatabase(path, version: 1, onCreate: _onCreate);

    return database;
  }

  _onCreate(Database db, int version) {
    db.execute('''
    create table $table ($id integer primary key autoincrement, $date text, $time text, $content text, $color text)
    ''');
  }

  void insertWork(Map error) async {
    var db = await this.database;
    await db.insert(table, error);
  }

  Future<List<Map>> getWorks() async {
    var db = await this.database;
    var result = await db.rawQuery('SELECT * FROM $table');
    return result;
  }

  // Future<List<Map>> login(String email, String password) async {
  //   var db = await this.database;
  //   var result = await db.rawQuery(
  //       '''SELECT * FROM $table WHERE email = '$email' and password = '$password' ''');
  //   return result;
  // }
}
