import 'package:sqflite/sqflite.dart';

class UserDatabase {
  static Database _database;
  static String table = 'User';
  static String id = 'id';
  static String email = 'email';
  static String password = 'password';
  static UserDatabase _userDatabase;

  UserDatabase._createInstance();

  factory UserDatabase() {
    if (_userDatabase == null) {
      _userDatabase = UserDatabase._createInstance();
    }
    return _userDatabase;
  }

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await initDB();
    return _database;
  }

  Future<Database> initDB() async {
    var dir = await getDatabasesPath();
    var path = dir + 'user.db';

    var database = await openDatabase(path, version: 1, onCreate: _onCreate);

    return database;
  }

  _onCreate(Database db, int version) {
    db.execute('''
    create table $table ($id integer primary key autoincrement, $email text, $password text)
    ''');
  }

  void insertUser(Map error) async {
    var db = await this.database;
    await db.insert(table, error);
  }

  Future<List<Map>> getUser() async {
    var db = await this.database;
    var result = await db.rawQuery('SELECT * FROM $table');
    return result;
  }

  Future<List<Map>> login(String email, String password) async {
    var db = await this.database;
    var result = await db.rawQuery(
        '''SELECT * FROM $table WHERE email = '$email' and password = '$password' ''');
    return result;
  }
}
