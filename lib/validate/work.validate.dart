import 'package:formz/formz.dart';

enum ContentValidationError { invalid }

class Content extends FormzInput<String, ContentValidationError> {
  const Content.pure([String value = '']) : super.pure(value);
  const Content.dirty([String value = '']) : super.dirty(value);

  @override
  ContentValidationError validator(String value) {
    return value == '' ? null : ContentValidationError.invalid;
  }
}
